import React, { useState, useEffect } from 'react';
import '../Navbar.css';
import { ReactComponent as Logo } from './giphy-logo-1.svg';

function Navbar() {
    return (
        <>
            <nav className='navbar'>
                <Logo height={70}/>
            </nav>
        </>
    );
}

export default Navbar;