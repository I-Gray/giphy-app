import React from 'react';
import Giphy from './components/Giphy';
import "./App.css";
import Navbar from './components/Navbar';

const App = () => {
    return (
        <div>
            <div><Giphy /></div>
        </div>
    );
};

export default App
