
import React, {useEffect, useState} from 'react';
import { FaSearch } from 'react-icons/fa';
import { ReactComponent as Logo } from './giphy-logo-1.svg';
import axios from 'axios';
import Loader from './Loader';
import '../App.css';
import '../Navbar.css';

const Giphy = () => {
    const [gifs, setData] = useState([]);
    const [search, setSearch] = useState("");
    const [gifsLoading, setGifsLoading] = useState(false);
    const [isError, setIsError] = useState(false);

    useEffect(() => {
        const apiCall = async () => {
            setIsError(false);
            setGifsLoading(false);

            try {
                const apiReturn = await axios("https://api.giphy.com/v1/gifs/trending", {
                    params: {
                        api_key: "rwNmNJnIugLUf2p0pcP8xJaTWHndQOwv",
                        limit: 100
                    }
                });

                console.log(apiReturn);
                setData(apiReturn.data.data);
            }

            catch (err) {
                setIsError(true);
                setTimeout(() => setIsError(false), 5000);
            }
        };

        setGifsLoading(true);
        apiCall()
    }, []);

    const loadGifs = () => {
        if (gifsLoading) {
            return <Loader />
        }
        return gifs.map( el => {
            return (
                <div key={el.id} className="gif">
                    <img src={el.images.fixed_height.url} />
                </div>
            )
        })
    };

    const renderError = () => {
        if (isError) {
            return (
                <div className="alert alert-danger alert-dismissible fade show" 
                    role="alert" >
                        There was a problem when attempting to fetch the Gifs.
                        Please try again later.
                        <button className="close"></button>
                </div>
            );
        }
    };

    const handleSearchChange = event => {
        setSearch(event.target.value)
    };

    const handleSubmit = async event => {
        event.preventDefault();
        setIsError(false);
        setGifsLoading(true);

        try {
            const apiReturn = await axios("https://api.giphy.com/v1/gifs/search", {
                params: {
                    api_key: "rwNmNJnIugLUf2p0pcP8xJaTWHndQOwv",
                    q: search,
                    limit: 50
                }      
            });
            setData(apiReturn.data.data);
        } catch (err) {
            setIsError(true);
            setTimeout(() => setIsError(false), 5000);
        }
        setGifsLoading(false);
    };

    return (
        <nav className='navbar'>
        <Logo height={60}/>
        <div className="m-2">
            {renderError()}
            <form className="form-inline justify-content-center m-2">
                <div class="input-group">
                <input onChange={handleSearchChange} type="text" placeholder="Search "
                class="form-control search-form " />
                
                <button onClick={handleSubmit} type="submit" class="search">
                    <FaSearch class="search-icon" />
                </button>
                </div>
            </form>
            
        </div>
        <div className="gifs">{loadGifs()}</div>
        </nav>
        
    )
};

export default Giphy